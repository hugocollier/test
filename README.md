## Important
x64 VM required to run OHP

## Usage

First, get the script and make it executable:

```bash
curl -O https://gitlab.com/hugocollier/test/-/raw/main/openvpn-install.sh
chmod +x openvpn-install.sh

curl -O  https://gitlab.com/hugocollier/test/-/raw/main/ohp-privoxy.sh
chmod +x ohp-privoxy.sh
 
```

Then run it:

```sh
./openvpn-install.sh
./ohp-privoxy.sh
```

OHP Config example
```text
client
proto tcp-client
remote "bughost.com" 1194
http-proxy "IPVPS" 9998 #(OHP Listening Port)
dev tun
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
verify-x509-name server_Usf33dLrtxf4g414 name
auth SHA256
auth-nocache
cipher AES-128-GCM
tls-client
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256
ignore-unknown-option block-outside-dns
setenv opt block-outside-dns # Prevent Windows 10 DNS leak
verb 3

<ca></ca>
<cert></cert>
<key></key>
<tls-crypt></tls-crypt>
```
